package exercise2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Exercise2Test {
  
  @Test
  public void formattedStringShouldBeCorrect(){
    Exercise2 exercise2 = new Exercise2("Exercise 2", "Exercise", "An exercise about private methods in interfaces.");
    String result = exercise2.toFormattedString();
    String expectedResult = "Name: Exercise 2\nType: Exercise\nDescription: An exercise about private methods in interfaces.\n";
    
    assertEquals(expectedResult, result);
  }

  @Test
  public void formattedStringShouldNotContainDescriptionWhenMissing(){
    Exercise2 exercise2 = new Exercise2("Exercise 2", "Exercise");
    String result = exercise2.toFormattedString();
    String expectedResult = "Name: Exercise 2\nType: Exercise\n";

    assertEquals(expectedResult, result);
  }

  @Test
  public void formattedStringShouldNotContainNameOrDescriptionWhenBlankOrMissing(){
    Exercise2 exercise2 = new Exercise2("    ", "Exercise");
    String result = exercise2.toFormattedString();
    String expectedResult = "Type: Exercise\n";

    assertEquals(expectedResult, result);
  }
}
