package exercise3;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class Exercise3_CollectionsTest {

  @Test
  public void getNameListTest() {
    var expectedResult = List.of("Paco", "Maria", "James", "John", "Rodrigo");
    var result = Exercise3_Collections.getNameList();

    assertTrue(result.size() == expectedResult.size()
            && result.containsAll(expectedResult)
            && expectedResult.containsAll(result));
  }

  @Test
  public void addNameAfterInitializationShouldContainAllThreeNames() {
    var expectedResult = List.of("James", "John", "Rodrigo");
    var result = Exercise3_Collections.addNameAfterInitialization("Rodrigo");

    assertTrue(result.size() == expectedResult.size()
            && result.containsAll(expectedResult)
            && expectedResult.containsAll(result));
  }

  @Test
  public void getUniqueIdsShouldHaveUniqueIds() {
    var expectedResult = List.of("123a", "231b");
    var result = Exercise3_Collections.getUniqueIds();

    assertTrue(result.size() == expectedResult.size()
            && result.containsAll(expectedResult)
            && expectedResult.containsAll(result));
  }
}
