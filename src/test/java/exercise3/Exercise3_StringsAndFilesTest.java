package exercise3;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Exercise3_StringsAndFilesTest {
  @Test
  public void resultStringFromFileShouldBeFormatted() {
    String result = Exercise3_StringsAndFiles.getFileFormattedString("assets/Exercise3.txt");

    String expected = "aaa_testtesttestlongwordlongwordlongword_splitsplitsplit_7";
    
    assertEquals(expected, result);
  }

  @Test
  public void repeatElementNTimesShouldCorrectlyRepeatGivenString() {
    String result = Exercise3_StringsAndFiles.repeatElementNTimes("word", 4);

    assertEquals("wordwordwordword", result);
  }
  
  @Test
  public void shouldWriteProvidedStringToFile() throws IOException {
    String expected = "This example string should have been written to the file.";
    String filePath = "assets/testExercise3.txt";
    Exercise3_StringsAndFiles.writeStringToFile(expected, filePath);

    String result = Files.readString(Path.of(filePath));
    // remove created file
    Files.delete(Path.of(filePath));
    
    assertEquals(expected, result);
  }
}
