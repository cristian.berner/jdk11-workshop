package exercise1;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Exercise1Test {

  @Test
  public void sumShouldBeCorrect() {
    int a = 10, b = 15;
    int expectedResult = 25;
    int result = Exercise1.calculateSum(a, b);

    assertEquals(expectedResult, result);
  }

  @Test
  public void readFruitsFromFileShouldReturnAListOfFruits() {
    // These tests 2 next lines may be simplified by using var
    List<String> expectedResult = List.of("Pomelo", "Cherry", "Banana", "Berry", "Orange");
    List<String> result = Exercise1.getFruitsFromFile("assets/Exercise1-test.txt");

    assertTrue(result.size() == expectedResult.size()
            && result.containsAll(expectedResult)
            && expectedResult.containsAll(result));
  }
}
