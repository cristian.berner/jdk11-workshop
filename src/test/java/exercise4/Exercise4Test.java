package exercise4;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Exercise4Test {
  List<Fruit> providedList;

  @BeforeEach
  public void initializeProvidedFruits() {
    providedList = List.of(
      new Fruit("Strawberry", FruitType.BERRY),
      new Fruit("Blackberry", FruitType.BERRY),
      new Fruit("Orangeberry", FruitType.BERRY),
      new Fruit("Pomegranate", FruitType.POME),
      new Fruit("Apple", FruitType.POME),
      new Fruit("Blueberry", FruitType.BERRY),
      new Fruit("Tomato", FruitType.LEGUMES));
  }

  @Test
  public void listOfBerryFruitsShouldBeCorrect() {
    List<Fruit> expectedResult = List.of(
      new Fruit("Strawberry", FruitType.BERRY),
      new Fruit("Blackberry", FruitType.BERRY),
      new Fruit("Orangeberry", FruitType.BERRY));

    List<Fruit> result = Exercise4.getFirstBerryFruits(providedList);
    assertTrue(result.size() == expectedResult.size()
      && result.containsAll(expectedResult)
      && expectedResult.containsAll(result));
  }

  @Test
  public void listOfFruitsShouldHaveFirstBerryFruitsRemovedBeCorrect() {
    List<Fruit> expectedResult = List.of(
      new Fruit("Pomegranate", FruitType.POME),
      new Fruit("Apple", FruitType.POME),
      new Fruit("Blueberry", FruitType.BERRY),
      new Fruit("Tomato", FruitType.LEGUMES));

    List<Fruit> result = Exercise4.removeFirstBerryFruitsFromList(providedList);
    assertTrue(result.size() == expectedResult.size()
      && result.containsAll(expectedResult)
      && expectedResult.containsAll(result));
  }
  
  @Test
  public void testNullableStreamIsAccepted() {
    assertDoesNotThrow(() -> Exercise4.removeFirstBerryFruitsFromList(null));
  }
}
