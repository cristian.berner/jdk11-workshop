package exercise3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Exercise3_Collections {
  // Collections factory methods
  // This is an example, it does not require modification
  public void ExampleListInit() {
    // old way
    List<String> colors = new ArrayList<>();
    colors.add("red");
    colors.add("green");
    colors.add("black");
    colors.add("orange");
    colors.add("blue");

    // New way
    colors = List.of("red", "green", "black", "orange", "blue");
    // Other collection methods, Set.of...
  }

  // Exercise: Transform to new initialization

  public static List<String> getNameList() {
    List<String> names = new ArrayList<>();
    names.add("Paco");
    names.add("Maria");
    names.add("James");
    names.add("John");
    names.add("Rodrigo");

    return names;
  }

  public static List<String> addNameAfterInitialization(String name) {
    List<String> names = new ArrayList<>();
    names.add("James");
    names.add("John");

    // After initialization, don't add this to the List.of, what happens?
    names.add(name);

    return names;
  }

  public static Set<String> getUniqueIds() {
    Set<String> ids = new HashSet<>();
    ids.add("123a");
    ids.add("231b");
    ids.add("123a");

    return ids;
  }
}
