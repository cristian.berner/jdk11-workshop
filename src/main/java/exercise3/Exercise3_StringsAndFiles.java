package exercise3;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class Exercise3_StringsAndFiles {
  // Make usage of repeat, isBlank, strip/stripLeading/stripTrailing and lines

  public static String repeatElementNTimes(String elem, int repeatTimes) {
    // TODO: Simplify using string.repeat utility method
    String repeatedResult = "";
    for (int i = 0; i < repeatTimes; i++) {
      repeatedResult += elem;
    }

    return repeatedResult;
  }

  /**
   * This method will read x file and format it like so: Reading each line
   * * if it is blank(contains only white spaces or nothing) add _ to result
   * * if it is not blank, strip all white spaces and repeat the word 3 times, then add to result
   * * at the end, add the number of lines read from the file
   * <p>
   * Example: a   (2 trailing white spaces after a)
   * word1
   * (blank line no letters but white spaces)
   * Result: aaaword1word1word1_3 (a repeated 3 times, word1 3 times, _ from blank line and 3 number of read lines)
   *
   * @return Formatted string
   */
  public static String getFileFormattedString(String fileName) {
    try {
      String readFile = Files.readString(Path.of(fileName));

      String result = "";
      // Make use of String utility methods, 
      // lines/isBlank/split/repeat which makes it easier than implementing these manually
      // TODO: Implement result transformation from readfile and applying what's described above on lines 24-27


      return result;
    } catch (IOException e) {
      e.printStackTrace();
    }

    return "";
  }

  /**
   * Writes input string to a filePath provided as is
   * @param input String input to be written in file
   */
  public static void writeStringToFile(String input, String filePath){
    File file = new File(filePath);
    
    // TODO: Transform to use Files.writeString method instead
    try (FileWriter fw = new FileWriter(file)){
      fw.write(input);      
    }catch(IOException e){
      e.printStackTrace();
    }
  }
}
