package exercise2;

/**
 * This Class doesn't require any modification.
 * Go to implemented FormattedToString interface and follow the exercises there
 */
public class Exercise2 implements FormattedToString {
  
  String name, type, description;
  
  public Exercise2(String name, String type, String description){
    this.name = name;
    this.type = type;
    this.description = description;
  }

  public Exercise2(String name, String type){
    this.name = name;
    this.type = type;
    this.description = null;
  }
  
  @Override
  public String getName() {
    return this.name;
  }

  @Override
  public String getType() {
    return this.type;
  }

  @Override
  public boolean hasDescription() {
    return this.description != null;
  }

  @Override
  public String getDescription() {
    return this.description;
  }
}
