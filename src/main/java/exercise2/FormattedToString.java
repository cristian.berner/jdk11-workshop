package exercise2;

/**
 * This interface provides a default toFormattedString method that has certain format distinct to Object's toString method
 */
public interface FormattedToString {

  public String getName();

  public String getType();

  public default boolean hasDescription() {
    return false;
  }

  public default String getDescription() {
    return this.hasDescription() ? "" : null;
  }

  /**
   * Default formatting method that will provide formatting by calling different implementable methods from this interface and Object
   *
   * @return Formatted string representing the object that implements this interface
   */
  public default String toFormattedString() {
    String name = getName();
    String type = getType();
    String description = getDescription();

    String formattedResult = "";
    if (!name.isBlank()) {
      formattedResult += "Name: " + name + "\n";
    }
    if (!type.isBlank()) {
      formattedResult += "Type: " + type + "\n";
    }
    if (description != null && !description.isBlank()) {
      formattedResult += "Description: " + description + "\n";
    }

    return formattedResult;
  }

  // Separate previous method to this private methods
  private String getFormattedName() {
    // Should return the formatted name, would do the name.isBlank check
    return "";
  }

  private String getFormattedType() {
    // Should return the formatted type, would do the name.isBlank check
    return "";
  }

  // TODO: Also implement a method for description similar to last 2 private ones.
}
