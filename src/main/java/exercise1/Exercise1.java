package exercise1;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * This is an exercise to learn about Local Variable Type Inference (var)
 * <p>
 * Objective: Change different parts to use var instead of direct Types
 */
public class Exercise1 {
  public static List<String> getFruitsFromFile(String fileName) {
    List<String> fruits = new ArrayList<>(); // What happens when we change List<String> to var <> operator

    try (FileInputStream fis = new FileInputStream(fileName);
         InputStreamReader isr = new InputStreamReader(fis);
         BufferedReader reader = new BufferedReader(isr)
    ) {
      String currentLine;
      while ((currentLine = reader.readLine()) != null) {
        fruits.add(currentLine);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }

    return fruits;
  }

  public static int calculateSum(int a, int b) { // Can we change parameters to var?
    return a + b;
  }

  public static short getZeroShort() {
    short zero = 0; // What happens if we change short to var?

    return zero;
  }
}
