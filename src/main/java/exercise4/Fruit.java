package exercise4;

import java.util.Objects;

public class Fruit {
  String name;
  FruitType type;

  public Fruit(String name, FruitType type) {
    this.name = name;
    this.type = type;
  }

  public String getName() {
    return name;
  }

  public FruitType getType() {
    return type;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Fruit fruit = (Fruit) o;
    return Objects.equals(name, fruit.name) && type == fruit.type;
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, type);
  }
}
