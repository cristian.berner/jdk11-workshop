package exercise4;

public enum FruitType {
  BERRY,
  DRUPE,
  POME,
  NUTS,
  LEGUMES
}
