package exercise4;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Exercises regarding Stream API new methods, <b>dropWhile</b> and <b>takeWhile</b> and <b>ofNullable</b>
 */
public class Exercise4 {

  /**
   * Will get the first set of berry fruits from a given list, until a non berry fruit is encountered
   * @param fruits Provided list of fruits to be filtered (assumed to be ordered)
   * @return first set of berry fruits from the provided List
   */
  public static List<Fruit> getFirstBerryFruits(List<Fruit> fruits){
    // TODO: Using Stream API, return the first set of berry fruits, until a non-berry fruit is encountered
    return fruits.stream().collect(Collectors.toList());
  }

  /**
   * Will remove the first set of berry fruits from a given list, until a non berry fruit is encountered
   * @param fruits Provided list of fruits to be filtered (assumed to be ordered)
   * @return fruits with the first series of berries fruits removed
   */
  public static List<Fruit> removeFirstBerryFruitsFromList(List<Fruit> fruits){
    // TODO: Using Stream API, return the fruit list with the first set of berry fruits removed
    // TODO: This should accept fruits parameter being null
    
    // BONUS: Experiment changing the order of the fruits using sort previous to dropWhile/takeWhile, what happens?
    return fruits.stream().collect(Collectors.toList());
  }
}
